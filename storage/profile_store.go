package storage

import (
	"cwtch.im/cwtch/peer"
)

// ProfileStore is an interface to managing the storage of Cwtch Profiles
type ProfileStore interface {
	Save(cwtchPeer peer.CwtchPeer) error
	Load() (peer.CwtchPeer, error)
}
