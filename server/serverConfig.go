package server

import (
	"crypto/rand"
	"encoding/json"
	"git.openprivacy.ca/openprivacy/libricochet-go/identity"
	"golang.org/x/crypto/ed25519"
	"io/ioutil"
	"log"
)

// Reporting is a struct for storing a the config a server needs to be a peer, and connect to a group to report
type Reporting struct {
	LogMetricsToFile    bool   `json:"logMetricsToFile"`
	ReportingGroupID    string `json:"reportingGroupId"`
	ReportingServerAddr string `json:"reportingServerAddr"`
}

// Config is a struct for storing basic server configuration
type Config struct {
	MaxBufferLines  int                `json:"maxBufferLines"`
	PublicKey       ed25519.PublicKey  `json:"publicKey"`
	PrivateKey      ed25519.PrivateKey `json:"privateKey"`
	ServerReporting Reporting          `json:"serverReporting"`
}

// Identity returns an encapsulation of the servers keys for running ricochet
func (config *Config) Identity() identity.Identity {
	return identity.InitializeV3("", &config.PrivateKey, &config.PublicKey)
}

// Save dumps the latest version of the config to a json file given by filename
func (config *Config) Save(filename string) {
	log.Println("Saving config to %s", filename)
	bytes, _ := json.MarshalIndent(config, "", "\t")
	ioutil.WriteFile(filename, bytes, 0600)
}

// LoadConfig loads a Config from a json file specified by filename
func LoadConfig(filename string) Config {
	log.Printf("Loading config from %s\n", filename)
	config := Config{}
	config.MaxBufferLines = 100000
	config.ServerReporting.LogMetricsToFile = false
	raw, err := ioutil.ReadFile(filename)
	if err == nil {
		err = json.Unmarshal(raw, &config)

		if err != nil {
			log.Println("Error reading config: ", err)
		}
	}

	if config.PrivateKey == nil {
		config.PublicKey, config.PrivateKey, _ = ed25519.GenerateKey(rand.Reader)
	}

	// Always save (first time generation, new version with new variables populated)
	config.Save(filename)
	return config
}
