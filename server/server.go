package server

import (
	"cwtch.im/cwtch/server/fetch"
	"cwtch.im/cwtch/server/listen"
	"cwtch.im/cwtch/server/metrics"
	"cwtch.im/cwtch/server/send"
	"cwtch.im/cwtch/storage"
	"git.openprivacy.ca/openprivacy/libricochet-go/application"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"log"
)

// Server encapsulates a complete, compliant Cwtch server.
type Server struct {
	app         *application.RicochetApplication
	config      Config
	metricsPack metrics.Monitors
}

// Run starts a server with the given privateKey
// TODO: surface errors
func (s *Server) Run(serverConfig Config) {
	s.config = serverConfig
	cwtchserver := new(application.RicochetApplication)
	s.metricsPack.Start(cwtchserver, s.config.ServerReporting.LogMetricsToFile)

	l, err := application.SetupOnionV3("127.0.0.1:9051", "tcp4", "", s.config.PrivateKey, utils.GetTorV3Hostname(s.config.PublicKey), 9878)

	if err != nil {
		log.Fatalf("error setting up onion service: %v", err)
	}

	af := application.ApplicationInstanceFactory{}
	af.Init()
	ms := new(storage.MessageStore)
	err = ms.Init(".", s.config.MaxBufferLines, s.metricsPack.MessageCounter)
	if err != nil {
		log.Fatal(err)
	}
	af.AddHandler("im.cwtch.server.listen", func(rai *application.ApplicationInstance) func() channels.Handler {
		return func() channels.Handler {
			cslc := new(listen.CwtchServerListenChannel)
			return cslc
		}
	})

	af.AddHandler("im.cwtch.server.fetch", func(rai *application.ApplicationInstance) func() channels.Handler {
		si := new(Instance)
		si.Init(rai, cwtchserver, ms)
		return func() channels.Handler {
			cssc := new(fetch.CwtchServerFetchChannel)
			cssc.Handler = si
			return cssc
		}
	})

	af.AddHandler("im.cwtch.server.send", func(rai *application.ApplicationInstance) func() channels.Handler {
		si := new(Instance)
		si.Init(rai, cwtchserver, ms)
		return func() channels.Handler {
			cssc := new(send.CwtchServerSendChannel)
			cssc.Handler = si
			return cssc
		}
	})

	cwtchserver.InitV3("cwtch server for "+l.Addr().String(), s.config.Identity(), af, new(application.AcceptAllContactManager))
	log.Printf("cwtch server running on cwtch:%s", l.Addr().String())
	s.app = cwtchserver
	s.app.Run(l)
}

// Shutdown kills the app closing all connections and freeing all goroutines
func (s *Server) Shutdown() {
	s.app.Shutdown()
	s.metricsPack.Stop()
}
