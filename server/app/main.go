package main

import (
	cwtchserver "cwtch.im/cwtch/server"
	"log"
	"os"
)

const (
	serverConfigFile = "serverConfig.json"
)

func main() {
	configDir := os.Getenv("CWTCH_CONFIG_DIR")

	serverConfig := cwtchserver.LoadConfig(configDir + serverConfigFile)

	server := new(cwtchserver.Server)
	log.Printf("starting cwtch server...")

	// TODO load params from .cwtch/server.conf or command line flag
	server.Run(serverConfig)
}
