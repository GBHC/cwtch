package metrics

import (
	"testing"
	"time"
)

func TestCounter(t *testing.T) {
	starttime := time.Now()
	c := NewCounter()

	for i := 0; i < 100; i++ {
		go func() {
			c.Add(1)
		}()
	}

	time.Sleep(2 * time.Second)

	val := c.Count()
	if val != 100 {
		t.Errorf("counter count was not 100")
	}

	counterStart := c.GetStarttime()

	if counterStart.Sub(starttime) > time.Millisecond {
		t.Error("counter's starttime was innaccurate")
	}
}
