package model

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"strconv"
	"testing"
	"time"
)

func TestMessagePadding(t *testing.T) {

	// Setup the Group
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroup("aaa.onion")
	gci := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite, gci)
	sarah.ProcessInvite(gci.GetGroupChatInvite(), alice.Onion)

	group := alice.GetGroupByGroupID(gid)

	c1, s1, err := sarah.EncryptMessageToGroup("Hello World 1", group.GroupID)
	t.Logf("Length of Encrypted Message: %v %v", len(c1), err)
	alice.AttemptDecryption(c1, s1)

	c2, s2, _ := alice.EncryptMessageToGroup("Hello World 2", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c2))
	alice.AttemptDecryption(c2, s2)

	c3, s3, _ := alice.EncryptMessageToGroup("Hello World 3", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c3))
	alice.AttemptDecryption(c3, s3)

	c4, s4, _ := alice.EncryptMessageToGroup("Hello World this is a much longer message 3", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c4))
	alice.AttemptDecryption(c4, s4)

}

func TestTranscriptConsistency(t *testing.T) {
	timeline := new(Timeline)

	// Setup the Group
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroup("aaa.onion")
	gci := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite, gci)
	sarah.ProcessInvite(gci.GetGroupChatInvite(), alice.Onion)

	group := alice.GetGroupByGroupID(gid)

	t.Logf("group: %v, sarah %v", group, sarah)

	c1, s1, _ := alice.EncryptMessageToGroup("Hello World 1", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c1))
	alice.AttemptDecryption(c1, s1)

	c2, s2, _ := alice.EncryptMessageToGroup("Hello World 2", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c2))
	alice.AttemptDecryption(c2, s2)

	c3, s3, _ := alice.EncryptMessageToGroup("Hello World 3", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c3))
	alice.AttemptDecryption(c3, s3)

	time.Sleep(time.Second * 1)

	c4, s4, _ := alice.EncryptMessageToGroup("Hello World 4", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c4))
	alice.AttemptDecryption(c4, s4)

	c5, s5, _ := alice.EncryptMessageToGroup("Hello World 5", group.GroupID)
	t.Logf("Length of Encrypted Message: %v", len(c5))

	_, m1 := sarah.AttemptDecryption(c1, s1)
	sarah.AttemptDecryption(c1, s1) // Try a duplicate
	_, m2 := sarah.AttemptDecryption(c2, s2)
	_, m3 := sarah.AttemptDecryption(c3, s3)
	_, m4 := sarah.AttemptDecryption(c4, s4)
	_, m5 := sarah.AttemptDecryption(c5, s5)

	// Now we simulate a client receiving these messages completely out of order
	timeline.Insert(m1)
	timeline.Insert(m5)
	timeline.Insert(m4)
	timeline.Insert(m3)
	timeline.Insert(m2)

	for i, m := range group.GetTimeline() {
		if m.Message != "Hello World "+strconv.Itoa(i+1) {
			t.Fatalf("Timeline Out of Order!: %v %v", i, m)
		}

		t.Logf("Messages %v: %v %x %x", i, m.Message, m.Signature, m.PreviousMessageSig)
	}
}
