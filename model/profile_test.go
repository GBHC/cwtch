package model

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"testing"
)

func TestProfileIdentity(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")

	message := sarah.GetCwtchIdentityPacket()

	ci := &protocol.CwtchPeerPacket{}
	err := proto.Unmarshal(message, ci)
	if err != nil {
		t.Errorf("alice should have added sarah as a contact %v", err)
	}

	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	if alice.Contacts[sarah.Onion].Name != "Sarah" {
		t.Errorf("alice should have added sarah as a contact %v", alice.Contacts)
	}

	if len(alice.GetContacts()) != 1 {
		t.Errorf("alice should be only contact: %v", alice.GetContacts())
	}

	alice.SetCustomAttribute("test", "hello world")
	value, _ := alice.GetCustomAttribute("test")
	if value != "hello world" {
		t.Errorf("value from custom attribute should have been 'hello world', instead was: %v", value)
	}

	t.Logf("%v", alice)
}

func TestTrustPeer(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	alice.TrustPeer(sarah.Onion)
	if alice.IsBlocked(sarah.Onion) {
		t.Errorf("peer should not be blocked")
	}

	if alice.TrustPeer("") == nil {
		t.Errorf("trusting a non existent peer should error")
	}
}

func TestBlockPeer(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	alice.BlockPeer(sarah.Onion)
	if !alice.IsBlocked(sarah.Onion) {
		t.Errorf("peer should not be blocked")
	}

	if alice.BlockPeer("") == nil {
		t.Errorf("blocking a non existent peer should error")
	}
}

func TestAcceptNonExistentGroup(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	sarah.AcceptInvite("doesnotexist")
}

func TestRejectGroupInvite(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroup("aaa.onion")
	gci := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite, gci)
	sarah.ProcessInvite(gci.GetGroupChatInvite(), alice.Onion)
	group := alice.GetGroupByGroupID(gid)
	if len(sarah.Groups) == 1 {
		if sarah.GetGroupByGroupID(group.GroupID).Accepted {
			t.Errorf("Group should not be accepted")
		}
		sarah.RejectInvite(group.GroupID)
		if len(sarah.Groups) != 0 {
			t.Errorf("Group %v should have been deleted", group.GroupID)
		}
		return
	}
	t.Errorf("Group should exist in map")
}

func TestProfileGroup(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroupWithMessage("aaa.onion", []byte("Hello World"))
	gci := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite, gci)
	sarah.ProcessInvite(gci.GetGroupChatInvite(), alice.Onion)
	if len(sarah.GetGroups()) != 1 {
		t.Errorf("sarah should only be in 1 group instead: %v", sarah.GetGroups())
	}

	group := alice.GetGroupByGroupID(gid)
	sarah.AcceptInvite(group.GroupID)
	c, s1, _ := sarah.EncryptMessageToGroup("Hello World", group.GroupID)
	alice.AttemptDecryption(c, s1)

	gid2, invite2, _ := alice.StartGroup("bbb.onion")
	gci2 := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite2, gci2)
	sarah.ProcessInvite(gci2.GetGroupChatInvite(), alice.Onion)
	group2 := alice.GetGroupByGroupID(gid2)
	c2, s2, _ := sarah.EncryptMessageToGroup("Hello World", group2.GroupID)
	alice.AttemptDecryption(c2, s2)

	sarahGroup := sarah.GetGroupByGroupID(group.GroupID)
	im := sarahGroup.GetInitialMessage()
	if string(im) != "Hello World" {
		t.Errorf("Initial Message was not stored properly: %v", im)
	}

	bob := GenerateNewProfile("bob")
	bob.AddContact(alice.Onion, &alice.PublicProfile)
	bob.ProcessInvite(gci2.GetGroupChatInvite(), alice.Onion)
	c3, s3, err := bob.EncryptMessageToGroup("Bobs Message", group2.GroupID)
	if err == nil {
		ok, message := alice.AttemptDecryption(c3, s3)
		if !ok {
			t.Errorf("Bobs message to the group should be decrypted %v %v", message, ok)
		}

		eve := GenerateNewProfile("eve")
		ok, _ = eve.AttemptDecryption(c3, s3)
		if ok {
			t.Errorf("Eves hould not be able to decrypt messages!")
		}
	} else {
		t.Errorf("Bob failed to encrypt a message to the group")
	}
}
