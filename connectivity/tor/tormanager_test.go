package tor

import (
	"fmt"
	"os"
	"os/exec"
	"testing"
)

func TestTorManager(t *testing.T) {
	tor, err := exec.LookPath("tor")
	if err != nil {
		t.Errorf("tor not found in PATH")
	}
	os.Remove("/tmp/torrc")
	file, _ := os.Create("/tmp/torrc")
	fmt.Fprintf(file, "SOCKSPort %d\nControlPort %d\nDataDirectory /tmp/tor\n", 10050, 10051)
	file.Close()
	tm, err := NewTorManager(10050, 10051, tor, "/tmp/torrc")
	if err != nil {
		t.Errorf("creating a new tor manager failed: %v", err)
	} else {

		tm2, err := NewTorManager(10050, 10051, tor, "/tmp/torrc")
		if err != nil {
			t.Errorf("creating a new tor manager failed: %v", err)
		}
		tm2.Shutdown() // should not noop
	}
	tm.Shutdown()
}
